//
//  ItemsViewController.h
//  iMDB
//
//  Created by WTJRAMIREZ on 4/12/15.
//  Copyright (c) 2015 WTJRAMIREZ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItemsViewController : UICollectionViewController <UISearchBarDelegate, UISearchDisplayDelegate, UISearchResultsUpdating, UICollectionViewDataSource, UIScrollViewDelegate>
    @property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
    @property (strong, nonatomic) NSMutableArray *series;

-(IBAction)goToSearch:(id)sender;
@end
