//
//  Season.m
//  iMDB
//
//  Created by WTJRAMIREZ on 4/12/15.
//  Copyright (c) 2015 WTJRAMIREZ. All rights reserved.
//

#import "Season.h"

@implementation Season
@synthesize episodes;
@synthesize idSeason;
@synthesize idSerie;

+(id)SeasonWithEpisodes:(NSMutableArray *)_episodes season:(NSInteger)_season{
    Season *newSeason = [[Season alloc] init];
    newSeason.episodes = _episodes;
    newSeason->idSeason = _season;
    return newSeason;
}
@end
