//
//  Season.h
//  iMDB
//
//  Created by WTJRAMIREZ on 4/12/15.
//  Copyright (c) 2015 WTJRAMIREZ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Season : NSObject{
    NSInteger idSeason;
    NSInteger idSerie;
    NSMutableArray *episodes;
}

@property (nonatomic, retain) NSMutableArray *episodes;
@property (nonatomic) NSInteger idSeason;
@property (nonatomic) NSInteger idSerie;

+(id)SeasonWithEpisodes:(NSMutableArray *)_episodes season:(NSInteger *)_season;

@end
