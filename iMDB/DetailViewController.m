//
//  DetailViewController.m
//  iMDB
//
//  Created by WTJRAMIREZ on 4/15/15.
//  Copyright (c) 2015 WTJRAMIREZ. All rights reserved.
//

#import <JLTMDbClient/JLTMDbClient.h>
#import "DetailViewController.h"

@interface DetailViewController (){
    NSInteger selectedSeason;
}
@end

@implementation DetailViewController
@synthesize serie;
@synthesize name;
@synthesize generos;
@synthesize actores;
@synthesize episodes;
@synthesize seasons;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSLog(@"serie: %@", serie);
    selectedSeason = 0;
    generos.delegate = self;
    actores.delegate = self;
    episodes.delegate = self;
    seasons.delegate = self;
    generos.dataSource = self;
    actores.dataSource = self;
    episodes.dataSource = self;
    seasons.dataSource = self;
    seasons.delegate = self;
    seasons.dataSource = self;
    [name setText:[serie name]];
    [self loadBasicData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == actores) {
        return serie.actors.count;
    }else if(tableView == generos){
        return serie.genres.count;
    }else if(tableView == episodes){
        if (serie.seasons.count > 0) {
            Season *season = [serie getSeasonWithId:selectedSeason];
            NSInteger val = season.episodes.count;
            return val;
        }
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CellIdentifier = @"CellActor";
    NSString *cellString = @"";
    if (tableView == actores) {
        CellIdentifier = @"CellActor";
        if(serie.actors.count > 0){
            cellString = [serie.actors objectAtIndex:indexPath.row];
        }
    }else if(tableView == generos){
        CellIdentifier = @"CellGenero";
        if(serie.genres.count > 0){
            cellString = [serie.genres objectAtIndex:indexPath.row];
        }
    }else if(tableView == episodes){
        CellIdentifier = @"CellEpisodio";
        Season *season = [serie getSeasonWithId:selectedSeason];
        if(season.episodes.count > 0){
            cellString = [season.episodes objectAtIndex:indexPath.row];
        }
    }
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.textLabel.text = cellString;
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return serie.seasons.count;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    Season *season = [serie.seasons objectAtIndex:indexPath.row];
    selectedSeason = [season idSeason];
    [self loadEpisodesWithId:selectedSeason];
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifer = @"CellTemporada";
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifer forIndexPath:indexPath];
    NSInteger IdSeason = [[serie.seasons objectAtIndex:indexPath.row] idSeason];
    UILabel *seasonlabel = (UILabel *)[cell viewWithTag:103];
    [seasonlabel setText: [NSString stringWithFormat:@"%ld" ,IdSeason]];
    return cell;
}

- (void)loadBasicData{
    NSString *section = [NSString stringWithFormat:@"tv/%d", [serie getIdSerie]];
    NSDictionary *dictParameters = @{};
    [[JLTMDbClient sharedAPIInstance] GET:section withParameters:dictParameters andResponseBlock:^(id response, NSError *error){
        if (!error) {
            NSLog(@"response detalle:%@", response);
            NSDictionary *dic = ((NSDictionary *) response);
            NSArray *genres = [dic objectForKey:@"genres"];
            NSMutableArray *newGenres = [[NSMutableArray alloc] init];
            NSMutableArray *newSeasons = [[NSMutableArray alloc] init];
            NSMutableArray *newActors = [[NSMutableArray alloc] init];
            for (NSDictionary *genre in genres) {
                [newGenres addObject:[genre objectForKey:@"name"]];
            }
            NSDictionary *dictSeasons = [dic objectForKey:@"seasons"];
            for (NSDictionary *season in dictSeasons) {
                NSInteger *val = [[season objectForKey:@"season_number"] integerValue];
                Season *season = [Season SeasonWithEpisodes:[NSMutableArray array] season:val];
                [newSeasons addObject:season];
            }
            [serie setSeasons:newSeasons];
            [serie setGenres:newGenres];
            [self.generos reloadData];
            [self.seasons reloadData];
            if(serie.seasons.count > 0){
                selectedSeason = [[serie.seasons objectAtIndex:0] idSeason];
                [self loadEpisodesWithId:(int)selectedSeason];
            }
        }else{
            NSLog(@"error loading detail");
        }
    }];
}

-(void)loadEpisodesWithId:(int)idSeason{
    NSString *section = [NSString stringWithFormat:@"tv/%d/season/%d", [serie getIdSerie], idSeason];
    NSDictionary *dictParameters = @{};
    [[JLTMDbClient sharedAPIInstance] GET:section withParameters:dictParameters andResponseBlock:^(id response, NSError *error){
        if (!error) {
            NSLog(@"response temporada:%@", response);
            NSDictionary *dic = ((NSDictionary *) response);
            NSArray *episodes = [dic objectForKey:@"episodes"];
            NSMutableArray *newEpisodes = [NSMutableArray array];
            for (NSDictionary *episode in episodes) {
                [newEpisodes addObject: [episode objectForKey:@"name"]];
            }
            if (serie.seasons.count > 0) {
                Season *season = [serie getSeasonWithId:idSeason];
                if (season.episodes.count == 0) {
                    season.episodes = newEpisodes;
                }else{
                    [season setEpisodes:newEpisodes];
                }
            }else{
                NSMutableArray *seasons = @[[Season SeasonWithEpisodes:newEpisodes season:idSeason]];
                serie.seasons = seasons;
            }
            [self.episodes reloadData];
        }else{
            NSLog(@"error loading seasons");
        }
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
