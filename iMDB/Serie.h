//
//  Film.h
//  iMDB
//
//  Created by WTJRAMIREZ on 4/12/15.
//  Copyright (c) 2015 WTJRAMIREZ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Serie : NSObject{
    int idSerie;
    NSString *name;
    NSMutableArray *genres;
    NSURL *image;
    NSMutableArray *actors;
    NSMutableArray *seasons;
}

@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSMutableArray *genres;
@property (nonatomic, retain) NSURL *image;
@property (nonatomic, retain) NSMutableArray *actors;
@property (nonatomic, retain) NSMutableArray *seasons;

+(id)initSerieOfGenre:(NSMutableArray *)genres actors:(NSMutableArray *)actors seasons:(NSMutableArray *)seasons name:(NSString *)name image:(NSURL *)image id:(int)_idSerie;

-(int)getIdSerie;
-(void)setIdSerie:(int)_id;
-(id)getSeasonWithId:(int)idSeason;
@end
