//
//  DetailViewController.h
//  iMDB
//
//  Created by WTJRAMIREZ on 4/15/15.
//  Copyright (c) 2015 WTJRAMIREZ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Serie.h"
#import "Season.h"

@interface DetailViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource>
@property (strong,nonatomic) Serie *serie;
@property (strong,nonatomic) IBOutlet UILabel *name;
@property (nonatomic,weak) IBOutlet UITableView *generos;
@property (nonatomic,weak) IBOutlet UITableView *actores;
@property (nonatomic,weak) IBOutlet UITableView *episodes;
@property (nonatomic,weak) IBOutlet UICollectionView *seasons;

@end
