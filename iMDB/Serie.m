//
//  Film.m
//  iMDB
//
//  Created by WTJRAMIREZ on 4/12/15.
//  Copyright (c) 2015 WTJRAMIREZ. All rights reserved.
//

#import "Serie.h"
#import "Season.h"

@implementation Serie
@synthesize name;
@synthesize genres;
@synthesize image;
@synthesize actors;
@synthesize seasons;

+(id)initSerieOfGenre:(NSMutableArray *)genres actors:(NSMutableArray *)actors seasons:(NSMutableArray *)seasons name:(NSString *)name image:(NSURL *)image id:(int)idSerie{
    Serie *newSerie = [[self alloc] init];
    newSerie.name = name;
    newSerie.genres = genres;
    newSerie.image = image;
    newSerie.seasons = seasons;
    newSerie.actors = actors;
    [newSerie setIdSerie:idSerie];
    return newSerie;
}

-(id)getSeasonWithId:(int)idSeason{
    for(Season *season in seasons){
        if(season.idSeason == idSeason){
            return season;
        }
    }
    return nil;
}

-(int)getIdSerie{
    return idSerie;
}

-(void)setIdSerie:(int)_id{
    idSerie = _id;
}

@end
