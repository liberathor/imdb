# README #

### iOS client for the movie db api ###

### Quick summary ###

  iOS app client from the api for the movie db.
  For more info got to www.themoviedb.org and the api documentation goto -> http://docs.themoviedb.apiary.io/#introduction/third-party-libraries

### Version 0.1 ###

  + Search tv series from name
  + View details of series tv
  + View details: actors, genres, seasons of serie tv
  + View names of episodes from a serie tv

### Summary of set up ###

  + Clone the repository
  + Setup cocoapods
  + Download dependencies 
  + Open workspace

### Configuration ###

  + Clone the repository from bitbucket -> git clone git@bitbucket.org:liberathor/imdb.git
  + Setup cocoapods for more documentation read: https://cocoapods.org
  + Download dependencies:
     Since your console go to folder project and type -> pod install
  + Open workspace with Xcode
     
### Dependencies ###

   requires iOS 7.1 and above.
   ILMovieDB 0.3 or superior
   AFNetworking 2.5.0 or superior.
   iMDB uses ARC.

### Contact ###

* Developed from Juan Carlos Ramirez Vargas
* Twitter: @liberahor 
* Linkedin: https://www.linkedin.com/profile/view?id=138033453&trk=hp-identity-photo
* email: juancarlosmillos21@gmail.com