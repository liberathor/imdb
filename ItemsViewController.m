//
//  ItemsViewController.m
//  iMDB
//
//  Created by WTJRAMIREZ on 4/12/15.
//  Copyright (c) 2015 WTJRAMIREZ. All rights reserved.
//

#import <JLTMDbClient/JLTMDbClient.h>
#import "ItemsViewController.h"
#import "Serie.h"
#import "Season.h"
#import "DetailViewController.h"

@interface ItemsViewController (){
    NSMutableArray *series;
    NSString *stringQuery;
    NSString *section;
    NSDictionary *dictParameters;
    NSNumber *totalPages;
    NSNumber *actualPage;
    NSInteger indexSelectedItem;
}

@end

@implementation ItemsViewController
@synthesize searchBar;

static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    totalPages = [[NSNumber alloc] initWithInt:0];
    actualPage = [[NSNumber alloc] initWithInt:1];
    series = [[NSMutableArray alloc] init];
    section = @"search/tv";
    self.searchDisplayController.displaysSearchBarInNavigationBar = YES;
    searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.collectionView.frame), 44)];
    searchBar.delegate = self;
    [searchBar sizeToFit];
    [searchBar setShowsScopeBar:NO];
    searchBar.autocorrectionType = UITextAutocorrectionTypeNo;
    [self.collectionView addSubview:searchBar];
    [self.collectionView setContentOffset:CGPointMake(0, 44)];
    [searchBar becomeFirstResponder];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return series.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifer = @"Cell";
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifer forIndexPath:indexPath];
    if (series.count > 0) {
        UILabel *title = (UILabel *)[cell viewWithTag:101];
        UIImageView *imageView = (UIImageView *)[cell viewWithTag:100];
        Serie *serie = [series objectAtIndex:indexPath.row];
        if (serie.image != nil) {
            dispatch_async(dispatch_get_global_queue(0,0), ^{
                NSData * data = [[NSData alloc] initWithContentsOfURL: serie.image];
                if ( data == nil )
                    return;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [imageView setImage:[UIImage imageWithData:data]];
                });
            });
        }
        [title setText:serie.name];
    }
    return cell;
}

#pragma mark Content Filtering
- (void) filterContentForSearchText:(NSString *) searchText scope:(NSString *) scope{
    [series removeAllObjects];
    stringQuery = searchText;
    dictParameters = @{@"query":stringQuery, @"page":actualPage};
    [[JLTMDbClient sharedAPIInstance] GET:section withParameters:dictParameters andResponseBlock:^(id response, NSError *error){
        if (!error) {
            NSLog(@"response: %@", response);
            NSDictionary *dic = ((NSDictionary *) response);
            NSArray *results = [dic objectForKey:@"results"];
            totalPages = [dic objectForKey:@"total_pages"];
            for (NSDictionary *result in results) {
                NSString *path = [result objectForKey:@"poster_path"];
                NSString *idSerie = [result objectForKey:@"id"];
                NSURL *imageUrl = nil;
                if (path!=nil) {
                    NSString *stringUrl = [NSString stringWithFormat:@"http://image.tmdb.org/t/p/w500%@", path];
                    imageUrl = [NSURL URLWithString:stringUrl];
                }
                NSMutableArray *tmp = [[NSMutableArray alloc] init];
                Serie *serie = [Serie initSerieOfGenre:tmp actors:tmp seasons:tmp name:[result objectForKey:@"name"] image:imageUrl id:[idSerie intValue]];
                [series addObject:serie];
            }
            [self.collectionView reloadData];
            [searchBar becomeFirstResponder];
        }else{
            NSLog(@"error in response: %@", response);
        }
    }];
}

- (void) loadNextPage{
    if (stringQuery!=nil) {
        if (actualPage < totalPages) {
            actualPage = [NSNumber numberWithInt:actualPage.intValue+1];
            dictParameters = @{@"query":stringQuery, @"page":actualPage};
            [[JLTMDbClient sharedAPIInstance] GET:section withParameters:dictParameters andResponseBlock:^(id response, NSError *error){
                if (!error) {
                    NSLog(@"response: %@", response);
                    NSDictionary *dic = ((NSDictionary *) response);
                    NSArray *results = [dic objectForKey:@"results"];
                    totalPages = [dic objectForKey:@"total_pages"];
                    for (NSDictionary *result in results) {
                        NSString *path = [result objectForKey:@"poster_path"];
                        NSString *idSerie = [result objectForKey:@"id"];
                        NSURL *imageUrl = nil;
                        if (path!=nil) {
                            NSString *stringUrl = [NSString stringWithFormat:@"http://image.tmdb.org/t/p/w500%@", path];
                            imageUrl = [NSURL URLWithString:stringUrl];
                        }
                        NSMutableArray *tmp = [[NSMutableArray alloc] init];
                        Serie *serie = [Serie initSerieOfGenre:tmp actors:tmp seasons:tmp name:[result objectForKey:@"name"] image:imageUrl id:[idSerie intValue]];
                        [series addObject:serie];
                    }
                    [self.collectionView reloadData];
                }else{
                    NSLog(@"error in response: %@", response);
                }
            }];
        }
    }
}

-(IBAction)goToSearch:(id)sender{
    [searchBar becomeFirstResponder];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if (![searchText isEqualToString:@""]) {
        [self filterContentForSearchText:searchText scope:nil];
        [self.collectionView setContentOffset:CGPointMake(0, 44)];
    }else{
        [series removeAllObjects];
        [self.collectionView reloadData];
        [searchBar becomeFirstResponder];
    }
}

- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope{
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat actualPosition = scrollView.contentOffset.y + 510;
    CGFloat contentHeigth = scrollView.contentSize.height - 185;
    if (contentHeigth > 0) {
        if(actualPosition >= contentHeigth){
            [self loadNextPage];
        }
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"item selected: %@", indexPath);
    indexSelectedItem = indexPath.row;
    [self performSegueWithIdentifier:@"detail" sender:collectionView];
}


- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([[segue identifier] isEqualToString:@"detail"]) {
        DetailViewController *detailViewController = (DetailViewController *) [segue destinationViewController];
        NSString *destinationTitle = [[series objectAtIndex:(int)indexSelectedItem] name];
        [detailViewController setTitle:destinationTitle];
        Serie *serie = [series objectAtIndex:indexSelectedItem];
        detailViewController.serie = serie;
    }
}

@end
